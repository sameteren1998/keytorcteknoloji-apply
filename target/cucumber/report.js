$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/main/java/com/keytorcteknoloji/uitestautomation/features/Keytorcteknoloji.feature");
formatter.feature({
  "name": "N11 Favorite Actions",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@omsgtests"
    }
  ]
});
formatter.scenario({
  "name": "Login",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@omsgtests"
    }
  ]
});
formatter.step({
  "name": "import all elements",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefination.import_all_elements()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "open browser with \"Chrome\"",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefination.open_browser_with_option(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "navigate page \"home_url\"",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefination.navigate_page_something(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "find element \"UserloginButton\"",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefination.find_element_something(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click element",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefination.click_element()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "find element \"email\"",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefination.find_element_something(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "write text element \"sameteren1998@gmail.com\"",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefination.write_text_element_something(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "find element \"password\"",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefination.find_element_something(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "write text element \"Major1432**\"",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefination.write_text_element_something(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "find element \"loginButton\"",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefination.find_element_something(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click element",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefination.click_element()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "find element \"searchBar\"",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefination.find_element_something(String)"
});
formatter.result({
  "error_message": "org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"css selector\",\"selector\":\"#searchData\"}\n  (Session info: chrome\u003d81.0.4044.113)\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.14.0\u0027, revision: \u0027aacccce0\u0027, time: \u00272018-08-02T20:19:58.91Z\u0027\nSystem info: host: \u0027DESKTOP-U78O6UB\u0027, ip: \u0027192.168.159.1\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_191\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 81.0.4044.113, chrome: {chromedriverVersion: 81.0.4044.69 (6813546031a4b..., userDataDir: C:\\Users\\SameTLp\\AppData\\Lo...}, goog:chromeOptions: {debuggerAddress: localhost:61286}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:virtualAuthenticators: true}\nSession ID: faae7a8724653bafbadd2e14824027b5\n*** Element info: {Using\u003did, value\u003dsearchData}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:548)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:322)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementById(RemoteWebDriver.java:368)\r\n\tat org.openqa.selenium.By$ById.findElement(By.java:188)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:314)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat org.openqa.selenium.support.events.EventFiringWebDriver.lambda$new$1(EventFiringWebDriver.java:105)\r\n\tat com.sun.proxy.$Proxy14.findElement(Unknown Source)\r\n\tat org.openqa.selenium.support.events.EventFiringWebDriver.findElement(EventFiringWebDriver.java:194)\r\n\tat com.keytorcteknoloji.uitestautomation.steps.StepDefination.find_element_something(StepDefination.java:131)\r\n\tat ✽.find element \"searchBar\"(src/main/java/com/keytorcteknoloji/uitestautomation/features/Keytorcteknoloji.feature:16)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "write text element \"Samsung\"",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefination.write_text_element_something(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "find element \"searchButton\"",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefination.find_element_something(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "click element",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefination.click_element()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "assert pageContains \"Samsung için\"",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefination.assert_pagecontains_something(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "find element \"secondPage\"",
  "keyword": "When "
});
formatter.match({
  "location": "StepDefination.find_element_something(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "click element",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefination.click_element()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "find element \"favoriteAddFor3thProduct\"",
  "keyword": "When "
});
formatter.match({
  "location": "StepDefination.find_element_something(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "click element",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefination.click_element()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "find element \"myFavorites\"",
  "keyword": "When "
});
formatter.match({
  "location": "StepDefination.find_element_something(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "click element",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefination.click_element()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "assert pageContains \"Favorilerim (1)\"",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefination.assert_pagecontains_something(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "find element \"favoritesDetail\"",
  "keyword": "When "
});
formatter.match({
  "location": "StepDefination.find_element_something(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "click element",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefination.click_element()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "find element \"deleteFavorite\"",
  "keyword": "When "
});
formatter.match({
  "location": "StepDefination.find_element_something(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "click element",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefination.click_element()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "assert pageContains \"İzlediğiniz bir ürün bulunmamaktadır.\"",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefination.assert_pagecontains_something(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "browser close",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefination.browser_close()"
});
formatter.result({
  "status": "skipped"
});
});