package com.keytorcteknoloji.uitestautomation;

import org.junit.internal.TextListener;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.keytorcteknoloji.uitestautomation.runtest.TestRunner;

@SpringBootApplication
public class UITestAutomationApplication implements CommandLineRunner {

	
	public static void main(String[] args) {
		SpringApplication.run(UITestAutomationApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {


		JUnitCore junit = new JUnitCore();
		junit.addListener(new TextListener(System.out));
		Result result = junit.run(TestRunner.class);
		if (result.wasSuccessful()) {
			System.out.println(
					"All tests SUCCESS. Test count: " + result.getRunCount() + ", Run time: " + result.getRunTime());
		} else {
			System.out.println(
					"Failed tests count: " + result.getFailureCount() + ", Test count: " + result.getRunCount());
		}
		
	        System.exit(202);
	}

	

}
